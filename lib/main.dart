import 'package:flutter/material.dart';

import 'package:flutter_mee/config.dart';
import 'package:flutter_mee/env/env.dart';

import 'package:flutter_mee/ui/ui.dart';

import 'package:flutter_mee/utils/keys.dart';

void main() => runApp(ConfigWrapper(
      config: Config.fromJson(env),
      child: Mee(),
    ));

class Mee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Stylish'),
      navigatorKey: Keys.navigatorKey,
      debugShowCheckedModeBanner: !ConfigWrapper.of(context).production,
      routes: <String, WidgetBuilder>{
        SplashScreen.routeName: (BuildContext context) {
          return SplashScreen(
            onInit: () async {
              await Future.delayed(Duration(seconds: 3));
              Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
            },
          );
        },
        LoginScreen.routeName: (BuildContext context) {
          return LoginScreen();
        },
        RegisterScreen.routeName: (BuildContext context) {
          return RegisterScreen();
        },
        HomeScreen.routeName: (BuildContext context) {
          return HomeScreen();
        }
      },
    );
  }
}
