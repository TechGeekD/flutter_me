import 'package:flutter/material.dart';
import 'package:flutter_mee/repository/auth_repository.dart';
import 'package:flutter_mee/ui/app_drawer/app_drawer.dart';
import 'package:flutter_mee/ui/auth/login_screen.dart';
import 'package:flutter_mee/ui/home/event_list_screen.dart';
import 'package:flutter_mee/ui/home/user_list_screen.dart';

class HomeScreen extends StatefulWidget {
  static final String routeName = '/home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentNavIndex = 0;
  final AuthRepository _authRepos = AuthRepository();

  final List<Widget> _navigationViews = [
    UserListScreen(),
    EventListScreen(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentNavIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _currentView = _navigationViews[_currentNavIndex];

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.teal,
        centerTitle: true,
        title: Text('Cloud Store List'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.white,
            ),
            iconSize: 30.0,
            tooltip: 'LogOut',
            onPressed: () async {
              final user = await _authRepos.signOut();
              if (user == null) {
                Navigator.of(context)
                    .pushReplacementNamed(LoginScreen.routeName);
              }
            },
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: _currentView,
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentNavIndex,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Users'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Text('Events'),
          ),
        ],
      ),
    );
  }
}
