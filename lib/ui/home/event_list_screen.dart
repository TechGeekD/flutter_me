import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter_mee/models/events.dart';

//import 'package:flutter_mee/models/user_votes.dart';
import 'package:flutter_mee/repository/event_repository.dart';

import 'package:flutter_mee/repository/user_repository.dart';

import 'package:flutter_mee/utils/utils.dart' as Utils;

class EventListScreen extends StatefulWidget {
  static final String routeName = '/eventList';
  static final String title = 'Event List';

  @override
  _EventListScreenState createState() => _EventListScreenState();
}

final _scaffoldKey = GlobalKey<ScaffoldState>();

class _EventListScreenState extends State<EventListScreen> {
  bool loading = false;
  final FocusNode _dialogUsernameFocus = FocusNode();
  final FocusNode _dialogVoteFocus = FocusNode();
  final TextEditingController _dialogUsernameController =
      TextEditingController();
  final TextEditingController _dialogVoteController = TextEditingController();

  final UserRepository _userRepo = UserRepository();
  final EventRepository _eventRepo = EventRepository();

  @override
  void initState() {
    super.initState();
    _loaderController(loader: false);
  }

  void dispose() {
    _dialogUsernameFocus.dispose();
    _dialogVoteFocus.dispose();
    _dialogUsernameController.dispose();
    _dialogVoteController.dispose();

    super.dispose();
  }

  _loaderController({bool loader = false}) {
    setState(() {
      loading = loader;
      print(loading.toString());
    });
  }

  _firestoreAddRecord() async {
    if (!loading) {
      Utils.LoadingDialog.show(context);
//      _loaderController(loader: true);
    }

//    final Map<String, dynamic> record = {
//      'name': _dialogUsernameController?.text,
//      'votes': int.parse(_dialogVoteController?.text),
//    };

    _dialogUsernameController.clear();
    _dialogVoteController.clear();

//    final id = record['name'].toLowerCase();
//    final resp = await _userRepo.addUserVote(id, record);
    await Future.delayed(Duration(seconds: 2));

    Utils.LoadingDialog.dismiss(context);
//    final message = resp.error ?? resp.message;
//    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  _firestoreUpdateUserVote(EventsData record, String name) async {
    if (!loading) {
      Utils.LoadingDialog.show(context);
//      _loaderController(loader: true);
    }

//    final Map<String, dynamic> reqData = {'name': name};
//    final Events resp = await _userRepo.updateUserVote(record.id, reqData);
    await Future.delayed(Duration(seconds: 2));

    Utils.LoadingDialog.dismiss(context);
//    final message = resp.error ?? resp.message;
//    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  _firestoreUpdateRecord(Map<String, dynamic> record) async {
    if (!loading) {
      Utils.LoadingDialog.show(context);
//      _loaderController(loader: true);
    }

//    final String id = record['id'];
    _dialogUsernameController.clear();
    _dialogVoteController.clear();

//    record.remove('id');
//    final UserVotes resp = await _userRepo.updateUserVote(id, record);
    await Future.delayed(Duration(seconds: 2));

    Utils.LoadingDialog.dismiss(context);
//    final message = resp.error ?? resp.message;
//    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  _firestoreDeleteUserVote(String userId) async {
    if (!loading) {
      Utils.LoadingDialog.show(context);
//      _loaderController(loader: true);
    }

//    final UserVotes resp = await _userRepo.deleteUserVote(userId);
    await Future.delayed(Duration(seconds: 2));
    Utils.LoadingDialog.dismiss(context);
//    final message = resp.error ?? resp.message;
//    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  @override
  Widget build(BuildContext context) {
    print('build');

    return Scaffold(
      key: _scaffoldKey,
      body: FutureBuilder(
        future: _eventRepo.getEventList(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('Something Went Wrong');
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            case ConnectionState.done:
              if (!snapshot.hasData) return LinearProgressIndicator();
              final documents = snapshot.data.data;

              return Container(
                child: ListView(
                  children: <Widget>[
                    ...documents.map(
                      (data) {
                        final EventsData record = data;

                        return Dismissible(
                          key: ValueKey(record.id),
                          onDismissed: (v) {
                            _firestoreDeleteUserVote(record.id);
                          },
                          background: Container(
                            color: Colors.red,
                          ),
                          child: Container(
                            margin: const EdgeInsets.all(12.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: InkWell(
                              onTap: () =>
                                  _firestoreUpdateUserVote(record, 'true'),
                              onDoubleTap: () =>
                                  _firestoreUpdateUserVote(record, 'false'),
                              onLongPress: () {
                                setState(() {
                                  _dialogUsernameController.text = record.name;
                                });
                                _showDialog(update: record.id);
                              },
                              child: ListTile(
                                key: ValueKey(record.name),
                                title: Text(record.name),
                                trailing:
                                    Text(record.participant.length.toString()),
                              ),
                            ),
                          ),
                        );
                      },
                    ).toList()
                  ],
                ),
              );
          }
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
          icon: Icon(Icons.add_circle_outline),
          label: Text('Add'),
          onPressed: () async {
            print('pop up');
            _showDialog();
          }),
    );
  }

  void _showDialog({update = false}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Add New UserVote"),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  autofocus: true,
                  textCapitalization: TextCapitalization.sentences,
                  controller: _dialogUsernameController,
                  focusNode: _dialogUsernameFocus,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(labelText: 'User\'s Name'),
                  onSubmitted: (v) {
                    print(v);
                    Utils.ChangeFocus(context,
                        current: _dialogUsernameFocus, next: _dialogVoteFocus);
                  },
                ),
                if (update == false)
                  TextFormField(
                    controller: _dialogVoteController,
                    focusNode: _dialogVoteFocus,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    textInputAction: TextInputAction.done,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    decoration: InputDecoration(labelText: 'Vote Count'),
                  )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                _dialogUsernameFocus.unfocus();
                _dialogVoteFocus.unfocus();
                _dialogUsernameController.clear();
                _dialogVoteController.clear();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Submit"),
              onPressed: () async {
                _dialogUsernameFocus.unfocus();
                _dialogVoteFocus.unfocus();
                Navigator.of(context).pop();
                if (update == false) {
                  _firestoreAddRecord();
                } else {
                  final record = {
                    'name': _dialogUsernameController?.text,
                    'id': update
                  };
                  _firestoreUpdateRecord(record);
                }
              },
            ),
          ],
        );
      },
    );
  }
}
