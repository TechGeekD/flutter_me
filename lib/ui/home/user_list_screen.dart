import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mee/models/user_votes.dart';

import 'package:flutter_mee/repository/user_repository.dart';

import 'package:flutter_mee/utils/utils.dart' as Utils;

class UserListScreen extends StatefulWidget {
  static final String routeName = '/home';
  static final String title = 'User List';

  @override
  _UserListScreenState createState() => _UserListScreenState();
}

final _scaffoldKey = GlobalKey<ScaffoldState>();

class _UserListScreenState extends State<UserListScreen> {
  final Stream<QuerySnapshot> _streamController = Firestore.instance
      .collection('users')
      .where('votes', isGreaterThanOrEqualTo: 0)
      .snapshots();
  bool loading = false;
  final FocusNode _dialogUsernameFocus = FocusNode();
  final FocusNode _dialogVoteFocus = FocusNode();
  final TextEditingController _dialogUsernameController =
      TextEditingController();
  final TextEditingController _dialogVoteController = TextEditingController();

  final UserRepository _userRepo = UserRepository();

  @override
  void initState() {
    super.initState();
    _loaderController(loader: true);
    _streamController.listen((onData) {
      _loaderController();
    });
  }

  void dispose() {
    _dialogUsernameFocus.dispose();
    _dialogVoteFocus.dispose();
    _dialogUsernameController.dispose();
    _dialogVoteController.dispose();

    super.dispose();
  }

  _loaderController({bool loader = false}) {
    setState(() {
      loading = loader;
      print(loading.toString());
    });
  }

  _firestoreAddRecord() async {
    Utils.LoadingDialog.show(context);
    if (!loading) {
      _loaderController(loader: true);
    }

    final Map<String, dynamic> record = {
      'name': _dialogUsernameController?.text,
      'votes': int.parse(_dialogVoteController?.text),
    };

    _dialogUsernameController.clear();
    _dialogVoteController.clear();

    final id = record['name'].toLowerCase();
    final resp = await _userRepo.addUserVote(id, record);

    Utils.LoadingDialog.dismiss(context);
    final message = resp.error ?? resp.message;
    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  _firestoreUpdateUserVote(UserVotesData record, bool add) async {
    Utils.LoadingDialog.show(context);
    if (!loading) {
      _loaderController(loader: true);
    }

    final int count = add == true ? 1 : -1;
    final Map<String, dynamic> reqData = {'votes': record.votes + count};
    final UserVotes resp = await _userRepo.updateUserVote(record.id, reqData);

    Utils.LoadingDialog.dismiss(context);
    final message = resp.error ?? resp.message;
    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  _firestoreUpdateRecord(Map<String, dynamic> record) async {
    Utils.LoadingDialog.show(context);
    if (!loading) {
      _loaderController(loader: true);
    }

    final String id = record['id'];
    _dialogVoteController.clear();
    _dialogUsernameController.clear();

    record.remove('id');
    final UserVotes resp = await _userRepo.updateUserVote(id, record);

    Utils.LoadingDialog.dismiss(context);
    final message = resp.error ?? resp.message;
    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  _firestoreDeleteUserVote(String userId) async {
    Utils.LoadingDialog.show(context);
    if (!loading) {
      _loaderController(loader: true);
    }

    final UserVotes resp = await _userRepo.deleteUserVote(userId);

    Utils.LoadingDialog.dismiss(context);
    final message = resp.error ?? resp.message;
    Utils.ShowSnackBar(_scaffoldKey, message: message);
  }

  @override
  Widget build(BuildContext context) {
    print('build');

    return Scaffold(
      key: _scaffoldKey,
      body: StreamBuilder<QuerySnapshot>(
        stream: _streamController,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return LinearProgressIndicator();
          final documents = snapshot.data.documents;

          return Container(
            child: ListView(
              children: <Widget>[
//                if (loading == true) LinearProgressIndicator(),
                ...documents.map(
                  (DocumentSnapshot data) {
                    final UserVotesData record =
                        UserVotesData.fromSnapshot(data);

                    return Dismissible(
                      key: ValueKey(record.id),
                      onDismissed: (v) {
                        _firestoreDeleteUserVote(record.id);
                      },
                      background: Container(
                        color: Colors.red,
                      ),
                      child: Container(
                        margin: const EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: InkWell(
                          onTap: () => _firestoreUpdateUserVote(record, true),
                          onDoubleTap: () =>
                              _firestoreUpdateUserVote(record, false),
                          onLongPress: () {
                            setState(() {
                              _dialogUsernameController.text = record.name;
                            });
                            _showDialog(update: record.id);
                          },
                          child: ListTile(
                            key: ValueKey(record.name),
                            title: Text(record.name),
                            trailing: Text(record.votes.toString()),
                          ),
                        ),
                      ),
                    );
                  },
                ).toList()
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
          icon: Icon(Icons.add_circle_outline),
          label: Text('Add'),
          onPressed: () async {
            print('pop up');
            _showDialog();
          }),
    );
  }

  void _showDialog({update = false}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Add New UserVote"),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  autofocus: true,
                  textCapitalization: TextCapitalization.sentences,
                  controller: _dialogUsernameController,
                  focusNode: _dialogUsernameFocus,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(labelText: 'User\'s Name'),
                  onSubmitted: (v) {
                    print(v);
                    Utils.ChangeFocus(context,
                        current: _dialogUsernameFocus, next: _dialogVoteFocus);
                  },
                ),
                if (update == false)
                  TextFormField(
                    controller: _dialogVoteController,
                    focusNode: _dialogVoteFocus,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    textInputAction: TextInputAction.done,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    decoration: InputDecoration(labelText: 'Vote Count'),
                  )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                _dialogUsernameFocus.unfocus();
                _dialogVoteFocus.unfocus();
                _dialogUsernameController.clear();
                _dialogVoteController.clear();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Submit"),
              onPressed: () async {
                _dialogUsernameFocus.unfocus();
                _dialogVoteFocus.unfocus();
                Navigator.of(context).pop();
                if (update == false) {
                  _firestoreAddRecord();
                } else {
                  final record = {
                    'name': _dialogUsernameController?.text,
                    'id': update
                  };
                  _firestoreUpdateRecord(record);
                }
              },
            ),
          ],
        );
      },
    );
  }
}
