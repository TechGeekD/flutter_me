import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({this.onInit});

  static final String routeName = '/';

  final Function onInit;

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    widget.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.event_seat),
              SizedBox(
                height: 10,
              ),
              Text('Hold Your Seat!'),
              SizedBox(
                height: 10,
              ),
              CircularProgressIndicator(),
            ],
          ),
        ),
      ),
    );
  }
}
