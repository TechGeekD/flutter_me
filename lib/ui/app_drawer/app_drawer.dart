import 'package:flutter/material.dart';

import 'package:flutter_mee/ui/auth/login_screen.dart';
import 'package:flutter_mee/ui/home/home_screen.dart';

//import 'package:flutter_mee/utils/utils.dart' as Utils;

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.teal,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      decoration: BoxDecoration(
                        color: Colors.teal,
                        border: Border.all(width: 2.0),
                      ),
                      accountName: Text(
                        'TechGeekD',
                        style: TextStyle(fontSize: 25.0),
                      ),
                      accountEmail: Text('TechGeekD@gmail.com'),
                      currentAccountPicture: CircleAvatar(
                        backgroundImage:
                            NetworkImage('https://i.pravatar.cc/150?img=58'),
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          Navigator.of(context)
                              .pushReplacementNamed(HomeScreen.routeName);
//                          Navigator.of(context).popUntil(
//                              ModalRoute.withName(HomeScreen.routeName));
//                        Utils.Keys.navigatorKey.currentState
//                            .popAndPushNamed(HomeScreen.routeName);
                        });
                      },
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        leading: Icon(Icons.home),
                        title: Text('Home'),
                        trailing: Opacity(
                          opacity: 0.8,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 20.0,
                          ),
                        ),
                      ),
                    ),
                    const Divider(height: 8.0),
                    FlatButton(
                      onPressed: () {
//                      Navigator.of(context)
//                          .pushNamed(EventListScreen.routeName);
                      },
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        leading: Icon(Icons.person),
                        title: Text('About Us'),
                        trailing: Opacity(
                          opacity: 0.8,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 20.0,
                          ),
                        ),
                      ),
                    ),
                    const Divider(height: 8.0),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context)
                            .pushReplacementNamed(LoginScreen.routeName);
                      },
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        leading: Icon(Icons.exit_to_app),
                        title: Text('Logout'),
                        trailing: Opacity(
                          opacity: 0.8,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 20.0,
                          ),
                        ),
                      ),
                    ),
                    const Divider(height: 8.0),
                  ],
                ),
              ),
              RaisedButton(
                color: Colors.tealAccent.withOpacity(0.1),
                onPressed: () {},
                child: ListTile(
                  title: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'v0.0.1',
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
