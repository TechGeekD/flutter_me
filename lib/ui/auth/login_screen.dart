import 'package:flutter/material.dart';

import 'package:flutter_mee/ui/home/home_screen.dart';
import 'package:flutter_mee/ui/auth/register_screen.dart';

import 'package:flutter_mee/repository/auth_repository.dart';

import 'package:flutter_mee/utils/utils.dart' as utils;

class LoginScreen extends StatefulWidget {
  static final String routeName = '/login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  FocusNode _hydraFocus;
  FocusNode _hellFocus;

  @override
  void initState() {
    super.initState();

    _hellFocus = FocusNode();
    _hydraFocus = FocusNode();
  }

  @override
  void dispose() {
    _hellFocus.dispose();
    _hydraFocus.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Login',
          style: TextStyle(color: Colors.black, fontSize: 30.0),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _Form(
                hellFocus: _hellFocus,
                hydraFocus: _hydraFocus,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: RaisedButton(
        onPressed: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Made With '),
            Icon(
              Icons.favorite,
              color: Colors.red,
            ),
            Text(' By TechGeekD'),
          ],
        ),
      ),
    );
  }
}

class _Form extends StatelessWidget {
  _Form({
    this.hellFocus,
    this.hydraFocus,
  });

  final FocusNode hydraFocus;
  final FocusNode hellFocus;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.black.withOpacity(0.2), width: 3),
        borderRadius: BorderRadius.circular(18),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 5.0,
            spreadRadius: 5.0,
          ),
        ],
      ),
      padding: const EdgeInsets.all(20),
      margin: const EdgeInsets.all(10),
      child: Container(
        child: Column(
          children: <Widget>[
            _Logo(),
            TextField(
              textInputAction: TextInputAction.next,
              focusNode: hellFocus,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                suffixIcon: Icon(Icons.whatshot),
                hintText: 'hell',
                helperText: 'hella',
              ),
              onSubmitted: (v) {
                utils.ChangeFocus(context,
                    current: hellFocus, next: hydraFocus);
              },
            ),
            TextField(
              textInputAction: TextInputAction.done,
              focusNode: hydraFocus,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                suffixIcon: Icon(Icons.security),
                hintText: 'hydra',
                helperText: 'secu',
              ),
              onSubmitted: (v) {
                Navigator.of(context)
                    .pushReplacementNamed(HomeScreen.routeName);
              },
            ),
            _ButtonBar(),
          ],
        ),
      ),
    );
  }
}

class _Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.black12, width: 3),
        shape: BoxShape.circle,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 5.0,
            spreadRadius: 5.0,
          ),
        ],
      ),
      margin: const EdgeInsets.all(20),
      child: Center(
        child: SizedBox(
          width: 150,
          height: 150,
          child: InkWell(
            onDoubleTap: () {
              Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
            },
            child: Image.network(
              'https://www.codemate.com/wp-content/uploads/2016/02/flutter-logo-round.png',
            ),
          ),
        ),
      ),
    );
  }
}

class _ButtonBar extends StatelessWidget {
  final AuthRepository _authRepos = AuthRepository();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            children: <Widget>[
              RaisedButton(
                color: Colors.deepPurple,
                splashColor: Colors.yellowAccent,
                onPressed: () async {
                  final user = await _authRepos.handleGoogleSignIn();
                  if (user != null) {
                    Navigator.of(context)
                        .pushReplacementNamed(HomeScreen.routeName);
                  }
                },
                child: Text(
                  'with Google',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              RaisedButton(
                color: Colors.deepPurple,
                splashColor: Colors.yellowAccent,
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(RegisterScreen.routeName);
                },
                child: Text(
                  'register',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              RaisedButton(
                color: Colors.deepPurple,
                splashColor: Colors.yellowAccent,
                onPressed: () {},
                child: Text(
                  'flutter mee away',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              RaisedButton(
                color: Colors.deepPurple,
                splashColor: Colors.yellowAccent,
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(HomeScreen.routeName);
                },
                child: Text(
                  'flutter mee away with you',
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
