import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthRepository {
  AuthRepository();

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<FirebaseUser> handleGoogleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user =
        await _firebaseAuth.signInWithCredential(credential);
    print("signed in " + user.displayName);

    return user;
  }

  Future<FirebaseUser> handleEmailPasswordSignIn({email, password}) async {
    final FirebaseUser user =
        await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    return user;
  }

  Future<FirebaseUser> getCurrentUser() async {
    final FirebaseUser currentUser = await _firebaseAuth.currentUser();

    return currentUser;
  }

  Future<FirebaseUser> signOut() async {
    await _firebaseAuth.signOut();
    await _googleSignIn.signOut();

    return getCurrentUser();
  }
}
