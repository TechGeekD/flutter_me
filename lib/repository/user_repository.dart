import 'package:flutter_mee/models/user_votes.dart';
import 'package:flutter_mee/services/api.dart';

class UserRepository {
  UserRepository() {
    this.request = API().request;
  }

  dynamic request;

  Future addUserVote(userId, data) async {
    final rawResponse = await request(
      method: METHODS.post,
      path: '/users/$userId',
      data: data,
    );

    final UserVotes response = UserVotes.fromJson(rawResponse);

    return response;
  }

  Future updateUserVote(userId, data) async {
    final rawResponse = await request(
      method: METHODS.put,
      path: '/users/$userId',
      data: data,
    );

    final UserVotes response = UserVotes.fromJson(rawResponse);

    return response;
  }

  Future deleteUserVote(userId) async {
    final rawResponse = await request(
      method: METHODS.delete,
      path: '/users/$userId',
    );

    final UserVotes response = UserVotes.fromJson(rawResponse);

    return response;
  }
}
