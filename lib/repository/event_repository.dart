import 'package:flutter_mee/models/events.dart';
import 'package:flutter_mee/services/api.dart';

class EventRepository {
  EventRepository() {
    this.request = API().request;
  }

  dynamic request;

  Future getEventList() async {
    final rawResponse = await request(
      method: METHODS.get,
      path: '/events',
    );

    final Events response = Events.fromJson(rawResponse);

    return response;
  }
}
