import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:flutter_mee/config.dart';
import 'package:flutter_mee/env/env.dart';

class METHODS {
  static final String get = 'GET',
      post = 'POST',
      put = 'PUT',
      delete = 'DELETE',
      patch = 'PATCH';
}

class API {
  final Config config = Config.fromJson(env);

  request({
    String method,
    String path,
    Map<String, String> query = const {},
    Map<String, dynamic> data = const {},
    Map<String, String> headers = const {
      HttpHeaders.contentTypeHeader: 'application/json',
    },
  }) async {
    final String contextPath = config.apiContext + path;

    final Uri uri = config.production == false
        ? Uri.http(config.apiUrl, contextPath, query)
        : Uri.https(config.apiUrl, contextPath, query);

    print('******** uri ******** $uri, ${jsonEncode(data)}');
    final request = http.Request(method.toLowerCase(), uri)
      ..headers.addAll(headers)
      ..body = jsonEncode(data);

    final stream = await request.send().then((onValue) => onValue.stream);

    final response =
        await stream.bytesToString().then((onData) => onData.toString());

    return jsonDecode(response);
  }
}
