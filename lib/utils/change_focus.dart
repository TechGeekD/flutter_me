import 'package:flutter/widgets.dart';

class ChangeFocus {
  ChangeFocus(this.context, {this.current, this.next}) {
    _changeFocus();
  }

  final BuildContext context;
  final FocusNode current, next;

  _changeFocus() {
    current.unfocus();
    FocusScope.of(context).requestFocus(next);
  }
}
