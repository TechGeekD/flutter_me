import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ShowSnackBar {
  ShowSnackBar(this._scaffoldKey, {this.message}) {
    _showSnackBar();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey;
  final String message;

  _showSnackBar() {
    final snackBar = SnackBar(
      content: Text(message),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
