import 'package:flutter/material.dart';

class LoadingDialog {
  static show(context, [loadingText = 'Updating...']) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => AlertDialog(
            content: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                Padding(padding: const EdgeInsets.all(10.0)),
                Text(loadingText),
              ],
            ),
          ),
    );
  }

  static dismiss(context) {
    Navigator.of(context).pop();
  }
}
