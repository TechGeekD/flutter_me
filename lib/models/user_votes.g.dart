// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_votes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserVotes _$UserVotesFromJson(Map<String, dynamic> json) {
  return UserVotes(
      status: json['status'] as int,
      data: json['data'] == null
          ? null
          : UserVotesData.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String,
      error: json['error'] as String);
}

Map<String, dynamic> _$UserVotesToJson(UserVotes instance) => <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
      'message': instance.message,
      'error': instance.error
    };

UserVotesData _$UserVotesDataFromJson(Map<String, dynamic> json) {
  return UserVotesData(
      votes: json['votes'] as int, name: json['name'] as String)
    ..reference = json['reference']
    ..id = json['id'] as String;
}

Map<String, dynamic> _$UserVotesDataToJson(UserVotesData instance) =>
    <String, dynamic>{
      'votes': instance.votes,
      'name': instance.name,
      'reference': instance.reference,
      'id': instance.id
    };
