// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'events.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Events _$EventsFromJson(Map<String, dynamic> json) {
  return Events(
      status: json['status'] as int,
      data: (json['data'] as List)
          ?.map((e) =>
              e == null ? null : EventsData.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      message: json['message'] as String)
    ..error = json['error'] as String;
}

Map<String, dynamic> _$EventsToJson(Events instance) => <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
      'message': instance.message,
      'error': instance.error
    };

EventsData _$EventsDataFromJson(Map<String, dynamic> json) {
  return EventsData(
      location: json['location'] == null
          ? null
          : Location.fromJson(json['location'] as Map<String, dynamic>),
      participant: (json['participant'] as List)
          ?.map((e) =>
              e == null ? null : User.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      organizer: json['organizer'] == null
          ? null
          : User.fromJson(json['organizer'] as Map<String, dynamic>),
      name: json['name'] as String,
      reference: json['reference'],
      id: json['id'] as String);
}

Map<String, dynamic> _$EventsDataToJson(EventsData instance) =>
    <String, dynamic>{
      'location': instance.location,
      'participant': instance.participant,
      'organizer': instance.organizer,
      'name': instance.name,
      'reference': instance.reference,
      'id': instance.id
    };

Location _$LocationFromJson(Map<String, dynamic> json) {
  return Location(lat: json['lat'] as int, long: json['long'] as int);
}

Map<String, dynamic> _$LocationToJson(Location instance) =>
    <String, dynamic>{'lat': instance.lat, 'long': instance.long};

User _$UserFromJson(Map<String, dynamic> json) {
  return User(name: json['name'] as String, votes: json['votes'] as int);
}

Map<String, dynamic> _$UserToJson(User instance) =>
    <String, dynamic>{'name': instance.name, 'votes': instance.votes};
