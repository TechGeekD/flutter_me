import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'events.g.dart';

Events eventsFromJson(String str) => Events.fromJson(json.decode(str));

String eventsToJson(Events data) => json.encode(data.toJson());

@JsonSerializable()
class Events {
  int status;
  List<EventsData> data;
  String message;
  String error;

  Events({
    this.status,
    this.data,
    this.message,
  });

  factory Events.fromJson(Map<String, dynamic> json) => _$EventsFromJson(json);

  Map<String, dynamic> toJson() => _$EventsToJson(this);
}

@JsonSerializable()
class EventsData {
  Location location;
  List<User> participant;
  User organizer;
  String name;
  dynamic reference;
  String id;

  EventsData(
      {this.location,
      this.participant,
      this.organizer,
      this.name,
      this.reference,
      this.id});

  factory EventsData.fromSnapshot(DocumentSnapshot snapshot) => EventsData.fromJson({
        ...Map<String, dynamic>.from(snapshot.data),
        'reference': snapshot.reference,
        'id': snapshot.documentID,
      });

  factory EventsData.fromJson(Map<String, dynamic> json) => _$EventsDataFromJson(json);

  Map<String, dynamic> toJson() => _$EventsDataToJson(this);
}

@JsonSerializable()
class Location {
  int lat;
  int long;

  Location({
    this.lat,
    this.long,
  });

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  Map<String, dynamic> toJson() => _$LocationToJson(this);
}

@JsonSerializable()
class User {
  String name;
  int votes;

  User({
    this.name,
    this.votes,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
