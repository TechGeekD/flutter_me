import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_votes.g.dart';

UserVotes userVotesFromJson(String str) => UserVotes.fromJson(json.decode(str));

String userVotesToJson(UserVotes data) => json.encode(data.toJson());

@JsonSerializable()
class UserVotes {
  int status;
  UserVotesData data;
  String message;
  String error;

  UserVotes({this.status, this.data, this.message, this.error});

  factory UserVotes.fromJson(Map<String, dynamic> json) =>
      _$UserVotesFromJson(json);

  Map<String, dynamic> toJson() => _$UserVotesToJson(this);
}

@JsonSerializable()
class UserVotesData {
  int votes;
  String name;
  dynamic reference;
  String id;

  UserVotesData({
    this.votes,
    this.name,
  });

  factory UserVotesData.fromSnapshot(DocumentSnapshot snapshot) => UserVotesData.fromJson({
        ...snapshot.data,
        'reference': snapshot.reference,
        'id': snapshot.documentID,
      });

  factory UserVotesData.fromJson(Map<String, dynamic> json) => _$UserVotesDataFromJson(json);

  Map<String, dynamic> toJson() => _$UserVotesDataToJson(this);
}
