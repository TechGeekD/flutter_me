import 'package:flutter/foundation.dart';

import 'package:flutter_mee/env/configs/dev.dart' as dev;
import 'package:flutter_mee/env/configs/prod.dart' as prod;

Map<String, dynamic> get env => kReleaseMode == true ? prod.config : dev.config;
