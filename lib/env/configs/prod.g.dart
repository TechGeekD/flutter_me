// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prod.dart';

// **************************************************************************
// JsonLiteralGenerator
// **************************************************************************

const _$configJsonLiteral = {
  'env': 'PROD',
  'production': true,
  'apiKey': '<PROD_KEY>',
  'apiUrl': 'fluttermee-a34a7.firebaseapp.com',
  'apiContext': '/api/v1'
};
